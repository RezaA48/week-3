@extends('layout.master')
@section('judul')
    <h1>Edit Cast</h1>
@endsection
@section('content')
<form action="/cast/{{ $cast->id }}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Cast</label>
      <input type="text" value="{{ $cast->nama }}" name="nama" class="form-control" aria-describedby="emailHelp">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label name="umur">Umur</label>
      <input type="text" value="{{ $cast->umur }}" name="umur" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <label for="bio">Bio</label><br><br>
    <textarea name="bio" class="form-control" id="" cols="30" rows="10">
        {{ $cast->bio }}
    </textarea><br>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection