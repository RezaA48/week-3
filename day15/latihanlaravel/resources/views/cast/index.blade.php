@extends('layout.master')
@section('judul')
    <h1>Halaman List Cast</h1>
@endsection
@section('content')
<a href="/cast/create" class="btn btn-primary bt-sn m-2">Tambah Cast</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse($cast as $key=>$item)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $item->nama }}</td>
                <td>{{ $item->umur }}</td>
                <td>{{ $item->bio }}</td>
                <td>
                    <form action="/cast/{{ $item->id }}" method="post">
                        @csrf
                        @method('delete')
                        <a href="/cast/{{ $item->id }}" class="btn btn-sm btn-info">Detail</a>
                        <a href="/cast/{{ $item->id }}/edit" class="btn btn-sm btn-warning">Edit</a>
                        <input type="submit" class="btn btn-sm btn-danger" value="delete">
                    </form>
                </td>
            </tr>
        @empty
        <tr>
            <td>Data Cast Kosong</td>
        </tr>
        @endforelse
    </tbody>
  </table>
  

@endsection