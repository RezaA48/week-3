<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');
Route::get('/data-table', function()
{
    return view('halaman.data-table');
});
Route::get('/table', function()
{
    return view('halaman.table');
});

//crud
Route::get('/cast/create', 'CastController@create');
//create
Route::post('/cast', 'CastController@Store');
//read
Route::get('/cast', 'CastController@Index');
//read per id
Route::get('/cast/{cast_id}','CastController@Show');
//update
Route::get('cast/{cast_id}/edit', 'CastController@Edit');
//update data berdasarkan ID di Table Cast
Route::put('/cast/{cast_id}', 'CastController@Update');
//delete
Route::delete('/cast/{cast_id}', 'CastController@Destroy');