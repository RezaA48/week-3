<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;


use Illuminate\Http\Request;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'nama' => 'required|max:50',
            'umur' => 'required',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'Nama Harus Di Isi (Tidak Boleh Kosong)',
            'umur.required' => 'Umur Harus Di Isi (Tidak Boleh Kosong',
            'bio.required' => 'Bio Harus Di Isi (Tidak Boleh Kosong',
        ]);

        DB::table('cast')->insert(
            [   'nama' => $request['nama'], 
                'umur' => $request['umur'],
                'bio' => $request['bio']
            ]
        );
        return redirect('/cast');
    }

    public function Index()
    {
        $cast = DB::table('cast')->get();
 
        return view('cast.index', compact('cast'));
    }

    public function Show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('cast'));
    }

    public function Edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));

    }

    public function Update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required|max:50',
            'umur' => 'required',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'Nama Harus Di Isi (Tidak Boleh Kosong)',
            'umur.required' => 'Umur Harus Di Isi (Tidak Boleh Kosong',
            'bio.required' => 'Bio Harus Di Isi (Tidak Boleh Kosong',
        ]);

        DB::table('cast')
              ->where('id', $id)
              ->update(['nama' => $request['nama'],
                        'umur' => $request['umur'],
                        'bio' => $request['bio']]);
        return redirect('/cast');
    }

    public function Destroy($id)
    {
        DB::table('cast')->where('id', '=', $id)->delete();
        return redirect('/cast');
    }
}
