<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('halaman.register');
    }

    public function welcome(Request $request)
    {
        $firstname = $request['firstname'];
        $lastname = $request['lastname'];
        $gender = $request['gender'];
        $nationality = $request['nationality'];
        $language = $request['language'];
        $bio = $request['bio'];

        return view('halaman.home', compact('firstname', 'lastname'));
    }
}
