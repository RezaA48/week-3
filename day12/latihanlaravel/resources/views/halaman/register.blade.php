<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label for="firstname">First name</label><br><br>
        <input type="text" name="firstname"><br><br>
        <label for="lastname">Last name</label><br><br>
        <input type="text" name="lastname"><br><br>
        <label for="gender">Gender:</label><br><br>
        <input type="radio" name="gender" value="Male">Male <br>
        <input type="radio" name="gender" value="Female">Female <br>
        <input type="radio" name="gender" value="other">Other <br><br>
        <label for="">Nationality :</label><br><br>
        <select name="nationality" id="">
            <option value="indonesian">Indonesian</option>
            <option value="singaporean">Singaporean</option>
            <option value="malaysian">Malaysian</option>
            <option value="australian">Australian</option>
        </select><br><br>
        <label name="language">Language Spoken:</label><br><br>
            <input type="checkbox" name="language" value="indonesian">Bahasa Indonesia <br>
            <input type="checkbox" name="language" value="english">English <br>
            <input type="checkbox" name="language" value="other">Other <br><br>
        <label for="bio">Bio</label><br><br>
            <textarea name="bio" id="" cols="30" rows="10">
            </textarea><br>
        <input type="submit">
    </form>
</body>
</html>